<?xml version="1.0" encoding="UTF-8"?>
<vars xmlns="http://www.idiominc.com/opentopic/vars">

    <!-- 

      This file controls strings that appear in the published output.  
      There is one version of this file per language.  To add support 
      for a new language, simply create a copy of this file and name 
      it using the <locale ISO code>.xml format. 
      
    -->

    <!-- 
       Strings used in the headers and footers of pages.
    -->

    <!-- Product name to be placed inside headers etc. -->
    <variable id="Product Name"/>

    <!-- The footer that appears on odd-numbered pages. -->
    <variable id="Body odd footer"><param ref-name="pagenum"/></variable>

    <!-- The footer that appears on even-numbered pages. -->
    <variable id="Body even footer"><param ref-name="pagenum"/></variable>

    <!-- The footer that appears on the first page of a chapter. -->
    <variable id="Body first footer"><param ref-name="pagenum"/></variable>

    <!-- The header that appears on the first page of a chapter. -->
    <variable id="Body first header"><param ref-name="prodname"/> > <param ref-name="heading"/></variable>

    <!-- The header that appears on odd-numbered pages. -->
    <variable id="Body odd header"><param ref-name="prodname"/> > <param ref-name="heading"/></variable>

    <!-- The header that appears on even-numbered pages. -->
    <variable id="Body even header"><param ref-name="prodname"/> > <param ref-name="heading"/></variable>

    <!-- The footer that appears on Preface odd-numbered pages. -->
    <variable id="Preface odd footer"><param ref-name="pagenum"/></variable>

    <!-- The footer that appears on Preface even-numbered pages. -->
    <variable id="Preface even footer"><param ref-name="pagenum"/></variable>

    <!-- The footer that appears on the first page of a Preface -->
    <variable id="Preface first footer"><param ref-name="pagenum"/></variable>
  
    <!-- The header that appears on Preface odd-numbered pages. -->
    <variable id="Preface odd header"><param ref-name="prodname"/> > Préface</variable>

    <!-- The header that appears on Preface even-numbered pages. -->
    <variable id="Preface even header"><param ref-name="prodname"/> > Préface</variable>

    <!-- The header that appears on Preface first page. -->
    <variable id="Preface first header"><param ref-name="prodname"/> > Préface</variable>

    <!-- The footer that appears on the odd-numbered table of contents pages. --> 
    <variable id="Toc odd footer"><param ref-name="pagenum"/></variable>

    <!-- The footer that appears on the even-numbered table of contents pages. --> 
    <variable id="Toc even footer"><param ref-name="pagenum"/></variable>

    <!-- The header that appears on the odd-numbered table of contents pages. -->
    <variable id="Toc odd header"><param ref-name="prodname"/> > <param ref-name="heading"/></variable>

    <!-- The header that appears on the odd-numbered table of contents pages. --> 
    <variable id="Toc even header"><param ref-name="prodname"/> > <param ref-name="heading"/></variable>

    <!-- The footer that appears on the odd-numbered index pages. -->
    <variable id="Index odd footer"><param ref-name="pagenum"/></variable>

    <!-- The footer that appears on the even-numbered index pages. -->
    <variable id="Index even footer"><param ref-name="pagenum"/></variable>

    <!-- The header that appears on the odd-numbered index pages. -->
    <variable id="Index odd header"><param ref-name="prodname"/> > <param ref-name="heading"/></variable>

    <!-- The header that appears on the even-numbered index pages. -->
    <variable id="Index even header"><param ref-name="prodname"/> > <param ref-name="heading"/></variable>

    <!-- The formatting used to display ordered lists (e.g., "1., 2." or "1), 2)" -->
    <variable id="Ordered List Number"><param ref-name="number"/>. </variable>
    <variable id="Ordered List Number 1"><param ref-name="number"/>. </variable>
    <variable id="Ordered List Number 2"><param ref-name="number"/>. </variable>
    <variable id="Ordered List Number 3"><param ref-name="number"/>. </variable>
    <variable id="Ordered List Number 4"><param ref-name="number"/>. </variable>
    <variable id="Ordered List Format 1">1</variable>
    <variable id="Ordered List Format 2">a</variable>
    <variable id="Ordered List Format 3">1</variable>
    <variable id="Ordered List Format 4">a</variable>

    <!-- The unordered list bullet   -->
    <variable id="Unordered List bullet">‣</variable>
    <variable id="Unordered List bullet 1">‣</variable>
    <variable id="Unordered List bullet 2">‣</variable>
    <variable id="Unordered List bullet 3">‣</variable>
    <variable id="Unordered List bullet 4">‣</variable> 

    <!-- The heading string to put at the top of the Table of Contents -->
    <variable id="Table of Contents">Sommaire</variable>

    <variable id="Index Continued String">suite</variable>
    <variable id="Index See String">, voir </variable>
    <variable id="Index See Also String">Voir aussi </variable>

    <!-- The string used to label a chapter within the table of contents. -->
    <variable id="Table of Contents Chapter">Chapitre <param ref-name="number"/> : </variable>

    <!-- The string used to label an appendix within the table of contents. -->
    <variable id="Table of Contents Appendix">Annexe <param ref-name="number"/> : </variable>

    <!-- The string used to label a part within the table of contents. -->
    <variable id="Table of Contents Part">Partie <param ref-name="number"/> : </variable>

    <!-- The string used to label a preface within the table of contents. -->
    <variable id="Table of Contents Preface">Préface </variable>

    <!-- The string used to label Notices within the table of contents. -->
    <variable id="Table of Contents Notices"/>

    <variable id="Preface title">Préface</variable>

    <!-- The heading to put at the top of a chapter when creating a chapter-level "mini-TOC" -->
    <variable id="Mini Toc">Sujets :</variable>

    <variable id="#note-separator"> : </variable>

    <!-- Text to use for 'Notice' label generated from <note> element. -->
    <variable id="Notice"><!--TODO:Notice--></variable>

    <!-- Text to use for figure titles. Renders as part of <fig>/<title>
          elements. -->
    <variable id="Figure.title">Illustration <param ref-name="number"/> : <param ref-name="title"/></variable>
    <variable id="Figure Number">Illustration <param ref-name="number"/></variable>

    <!-- Text to use for table titles. Renders as part of <table>/<title> elements. -->
    <variable id="Table.title">Tableau <param ref-name="number"/> : <param ref-name="title"/></variable>
    <variable id="Table Number">Tableau <param ref-name="number"/></variable>

    <!-- Text to use for related links label.
         Renders as part of <related-links> element. -->
    <variable id="Related Links">Liens connexes</variable>

    <!-- Default cross-reference text prefix. Used to generate reference 
         text in case it's not specified in source. -->
    <variable id="Cross-Reference">Référence croisée vers :</variable>

    <!-- Prefix for unresolved content reference. Used to generate reference 
         text in case target cannot be resolved. -->
    <variable id="Content-Reference">Référence de contenu vers :</variable>

    <!-- Default title text for referenced section title. Used to generate 
         reference text in case reference has no explicit content and
         referenced section has no title. -->
    <!-- Deprecated since 2.3 -->
    <variable id="Untitled section">Section sans titre.</variable>

    <!-- Default title text for referenced list item. Used to generate
         reference text in case reference has no explicit content and 
         reference target is a list item. -->
    <variable id="List item">Articles.</variable>

    <!-- Default title text for referenced footnote. Used to generate 
         reference text in case reference has no explicit content and 
         reference target is a footnote. -->
    <variable id="Foot note">Note de bas de page.</variable>

    <!-- Text to use for a generated label of navigation title. Renders
         as part of <navtitle> element. -->
    <variable id="Navigation title">Titre de navigation</variable>

    <!-- Text to use for a generated label of search title. Renders 
         as part of <searchtitle> element. -->
    <!-- Deprecated since 2.3 -->
    <variable id="Search title">Titre de recherche</variable>

    <!-- Text to use for chapter titles.-->
    <variable id="Chapter with number">Chapitre <param ref-name="number"/></variable>

    <!-- Text to use for appendix titles.-->
    <variable id="Appendix with number">Annexe <param ref-name="number"/></variable>

    <!-- Text to use for part titles.-->
    <variable id="Part with number">Partie <param ref-name="number"/></variable>

    <!-- Text to use for a label generated from <required-cleanup> element. -->
    <variable id="Required-Cleanup">[Nettoyage nécessaire]</variable>

    <!-- Template for generated page number of a reference 
         in printed manual. -->
    <variable id="On the page"> [<param ref-name="pagenum"/>]</variable>

    <!-- Template for generated page number of a reference
         in printed manual, when referenced element has no title. -->
    <variable id="Page"> [<param ref-name="pagenum"/>]</variable>

    <!-- Template for generated page number of a reference
         in chm/html, when referenced element has no title. -->
    <variable id="This link">ce lien</variable>

    <!-- Image path to use for a note of "note" type. -->
    <variable id="note Note Image Path"></variable>
    <!-- Image path to use for a note of "notice" type. -->
    <variable id="notice Note Image Path"></variable>
    <!-- Image path to use for a note of "attention" type. -->
    <variable id="attention Note Image Path">Configuration/OpenTopic/cfg/common/artwork/warning.gif</variable>

    <!-- Image path to use for a note of "caution" type. -->
    <variable id="caution Note Image Path">Configuration/OpenTopic/cfg/common/artwork/warning.gif</variable>

    <!-- Image path to use for a note of "danger" type. -->
    <variable id="danger Note Image Path">Configuration/OpenTopic/cfg/common/artwork/warning.gif</variable>
    <!-- Image path to use for a note of "warning" type. -->
    <variable id="warning Note Image Path">Configuration/OpenTopic/cfg/common/artwork/warning.gif</variable>
    <!-- Image path to use for a note of "fastpath" type. -->
    <variable id="fastpath Note Image Path"></variable>

    <!-- Image path to use for a note of "important" type. -->
    <variable id="important Note Image Path"></variable>

    <!-- Image path to use for a note of "remember" type. -->
    <variable id="remember Note Image Path"></variable>

    <!-- Image path to use for a note of "restriction" type. -->
    <variable id="restriction Note Image Path"></variable>

    <!-- Image path to use for a note of "tip" type. -->
    <variable id="tip Note Image Path"></variable>

    <!-- Image path to use for a note of "other" type. -->
    <variable id="other Note Image Path"></variable>

    <!--
       Strings used in the online-help publishing.
    -->

    <!-- The title of the "contents" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Contents button title">Sommaire</variable>

    <!-- The title of the "index" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Index button title">Index</variable>

    <!-- The title of the "search" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Search button title">Recherche</variable>

    <!-- The title of the "back" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Back button title">Retournez</variable>

    <!-- The title of the "forward" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Forward button title">Allez En avant</variable>

    <!-- The title of the "main page" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Main page button title">Page Principale</variable>

    <!-- The title of the "previous page" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Previous page button title">Page Précédente</variable>

    <!-- The title of the "next page" button in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Next page button title">Prochaine Page</variable>

    <!-- The "online help" document title prefix in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Online help prefix">Aide En ligne</variable>

    <!-- The search by index field title in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Search index field title">Recherche du mot-clé :</variable>

    <!-- The search by index button title in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Search index button title">Regardez</variable>

    <!-- The search by text field title in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Search text field title">Aide de recherche pour :</variable>

    <!-- The search by text button title in online-help output -->
    <!-- Deprecated since 2.3 -->
    <variable id="Search text button title">Recherche</variable>
    
    <!--Label for a step with importance="optional"-->
    <variable id="Optional Step">Facultatif :</variable>
    <variable id="Required Step">Obligatoire :</variable>

    <!--Labels for task sections, now reused from common-->
    <variable id="Task Prereq"></variable>
    <variable id="Task Context"></variable>
    <variable id="Task Steps"></variable>
    <variable id="#steps-unordered-label"></variable>
    <variable id="Task Result"></variable>
    <variable id="Task Example"></variable>
    <variable id="Task Postreq"></variable>
    <variable id="Step Number"><param ref-name="number"/>. </variable>
    <variable id="Step Format">1</variable>
    <variable id="Substep Number"><param ref-name="number"/>) </variable>
    <variable id="Substep Format">a</variable>
    <variable id="Glossary odd footer"><param ref-name="pagenum"/></variable>
    <variable id="Glossary even footer"><param ref-name="pagenum"/></variable>
    <variable id="Glossary odd header"><param ref-name="prodname"/> > <param ref-name="heading"/> | </variable>
    <variable id="Glossary even header"><param ref-name="prodname"/> > <param ref-name="heading"/> | </variable>
    <variable id="Glossary">Glossaire</variable>
    <variable id="List of Tables">Liste des tableaux</variable>
    <variable id="List of Figures">Liste des illustrations</variable>

  <variable id="#quote-start"><variable id="OpenQuote"/></variable>
  <variable id="#quote-end"><variable id="CloseQuote"/></variable>
</vars>