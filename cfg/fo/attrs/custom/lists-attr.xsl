<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

  <xsl:attribute-set name="dl">
    <xsl:attribute name="width">100%</xsl:attribute>
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0</xsl:attribute>
    <xsl:attribute name="space-after.minimum">4pt</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="dl__body">
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0</xsl:attribute>
    <xsl:attribute name="space-after">0</xsl:attribute>
    <xsl:attribute name="start-indent">0</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="dl.dlhead">
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0</xsl:attribute>
    <xsl:attribute name="space-after">0</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="dlentry">
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0pt</xsl:attribute>
    <xsl:attribute name="space-after">0pt</xsl:attribute>
    <xsl:attribute name="keep-together.within-page">always</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="dlentry.dt">
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0</xsl:attribute>
    <xsl:attribute name="space-after">0</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="dlentry.dt__content">
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0</xsl:attribute>
    <xsl:attribute name="space-after">0</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="dlentry.dd">
    <xsl:attribute name="border-start-style">solid</xsl:attribute><!--visual debug-->
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0</xsl:attribute>
    <xsl:attribute name="space-after">0</xsl:attribute>
    <xsl:attribute name="start-indent">4em</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="dlentry.dd__content">
    <xsl:attribute name="padding">0</xsl:attribute>
    <xsl:attribute name="space-before">0</xsl:attribute>
    <xsl:attribute name="space-after">0</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="dl.dlhead__row">
  </xsl:attribute-set>

  <xsl:attribute-set name="dlhead.dthd__cell">
  </xsl:attribute-set>

  <xsl:attribute-set name="dlhead.dthd__content">
  </xsl:attribute-set>

  <xsl:attribute-set name="dlhead.ddhd__cell">
  </xsl:attribute-set>

  <xsl:attribute-set name="dlhead.ddhd__content">
  </xsl:attribute-set>

</xsl:stylesheet>
