<?xml version='1.0'?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  version="2.0">

  <xsl:attribute-set name="topic">
    <xsl:attribute name="break-before">page</xsl:attribute>
    <xsl:attribute name="space-before">0pt</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="normalized.topic.title">
    <xsl:attribute name="padding-top">0</xsl:attribute>
    <xsl:attribute name="line-height">140%</xsl:attribute>
    <xsl:attribute name="space-before">0pt</xsl:attribute>
    <xsl:attribute name="space-after">6pt</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="topic.title" use-attribute-sets="common.title common.border__bottom normalized.topic.title">
  </xsl:attribute-set>

  <xsl:attribute-set name="topic.topic.title" use-attribute-sets="common.title common.border__bottom normalized.topic.title">
  </xsl:attribute-set>

  <xsl:attribute-set name="topic.topic.topic.title" use-attribute-sets="common.title normalized.topic.title">
  </xsl:attribute-set>

  <xsl:attribute-set name="topic.topic.topic.topic.title" use-attribute-sets="base-font common.title normalized.topic.title">
  </xsl:attribute-set>
  
  <xsl:attribute-set name="topic.topic.topic.topic.topic.title" use-attribute-sets="base-font common.title normalized.topic.title">
  </xsl:attribute-set>
  
  <xsl:attribute-set name="lq" use-attribute-sets="common.block">
    <xsl:attribute name="font-style">italic</xsl:attribute>
    <xsl:attribute name="start-indent">20pt + from-parent(start-indent)</xsl:attribute>
    <xsl:attribute name="padding-start">10pt</xsl:attribute>
    <xsl:attribute name="end-indent">16pt + from-parent(end-indent)</xsl:attribute>
    <xsl:attribute name="border-start-style">solid</xsl:attribute>
    <xsl:attribute name="border-start-width">6pt</xsl:attribute>
    <xsl:attribute name="border-color">#DADADA</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="pre" use-attribute-sets="base-font common.block">
    <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
    <xsl:attribute name="white-space-collapse">false</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="font-family">monospace</xsl:attribute>
    <xsl:attribute name="line-height">100%</xsl:attribute>
    <xsl:attribute name="font-size">0.7em</xsl:attribute>
  </xsl:attribute-set>
    
</xsl:stylesheet>
