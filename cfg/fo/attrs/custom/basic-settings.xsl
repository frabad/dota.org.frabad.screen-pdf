<?xml version='1.0'?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
>  
  <xsl:variable name="page-margins">25mm</xsl:variable>
  <xsl:variable name="page-width">210mm</xsl:variable>
  <xsl:variable name="page-height">148mm</xsl:variable><!--297mm-->
  <xsl:variable name="default-font-size">16pt</xsl:variable>
  <xsl:variable name="default-line-height">20pt</xsl:variable>
  
  <xsl:param name="tocMaximumLevel">2</xsl:param>
    
</xsl:stylesheet>
