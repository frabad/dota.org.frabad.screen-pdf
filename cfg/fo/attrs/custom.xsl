<?xml version='1.0'?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
>
  
  <xsl:import href="custom/basic-settings.xsl" />
  <xsl:import href="custom/front-matter-attr.xsl" />
  <xsl:import href="custom/topic-attr.xsl" />
  <xsl:import href="custom/pr-domain-attr.xsl" />
  <xsl:import href="custom/lists-attr.xsl" />
  <xsl:import href="custom/tables-attr.xsl" />
  <xsl:import href="custom/static-content-attr.xsl" />
  
</xsl:stylesheet>