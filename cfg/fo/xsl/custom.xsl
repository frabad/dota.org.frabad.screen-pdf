<?xml version='1.0'?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function"
>
    
  <xsl:import href="custom/front-matter.xsl"/>
  <xsl:import href="custom/lists.xsl"/>
  <xsl:import href="custom/links.xsl"/>
  <xsl:import href="custom/topic.xsl"/>
  
</xsl:stylesheet>