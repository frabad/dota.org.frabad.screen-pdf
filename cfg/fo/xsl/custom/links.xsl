<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:opentopic-mapmerge="http://www.idiominc.com/opentopic/mapmerge"
  xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function"
  xmlns:related-links="http://dita-ot.sourceforge.net/ns/200709/related-links"
  xmlns:dita-ot="http://dita-ot.sourceforge.net/ns/201007/dita-ot"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="dita-ot opentopic-mapmerge opentopic-func related-links xs"
  version="2.0">

<xsl:param name="desc.in.related-links" select="false()" />

<xsl:template name="insertLinkShortDesc">
  <xsl:param name="destination"/>
  <xsl:param name="element"/>
  <xsl:param name="linkScope"/>
  <xsl:choose>
    <!-- User specified description (from map or topic): use that. -->
    <xsl:when test="*[contains(@class,' topic/desc ')] and
                    processing-instruction()[name()='ditaot'][.='usershortdesc']">
        <fo:block xsl:use-attribute-sets="link__shortdesc">
            <xsl:apply-templates select="*[contains(@class, ' topic/desc ')]"/>
        </fo:block>
    </xsl:when>
    <!-- External: do not attempt to retrieve. -->
    <xsl:when test="$linkScope='external'"></xsl:when>
    <!-- When the target has a short description and no local override, use the target -->
    <xsl:when test="$desc.in.related-links and $element/*[contains(@class, ' topic/shortdesc ')]">
      <xsl:variable name="generatedShortdesc" as="element()*">
        <xsl:apply-templates select="$element/*[contains(@class, ' topic/shortdesc ')]"/>
      </xsl:variable>
      <fo:block xsl:use-attribute-sets="link__shortdesc">
        <xsl:apply-templates select="$generatedShortdesc" mode="dropCopiedIds"/>
      </fo:block>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
