<?xml version='1.0'?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function"
    xmlns:my="namespace:my"
    version="2.0">

  <xsl:variable name="mapmeta" select="$map/*[contains(@class, ' map/topicmeta ')]" />
    
  <xsl:template name="createFrontCoverContents">
    <xsl:variable name="prodname"
                  select="$mapmeta//*[contains(@class,' topic/prodname ')]" 
    />
    <xsl:variable name="cover-image">
      <xsl:call-template name="my:cover-image" />
    </xsl:variable>
    
    <!-- set the front cover image -->
    <xsl:if test="$cover-image">
      <fo:block xsl:use-attribute-sets="__frontmatter__image__block">
        <xsl:sequence select="$cover-image"/>
      </fo:block>
    </xsl:if>
    
    <!-- set the prodname -->
    <xsl:if test="$prodname">
      <fo:block xsl:use-attribute-sets="__frontmatter__prodname">
        <xsl:value-of select="$prodname"/>
      </fo:block>
    </xsl:if>
    
    <!-- set the title -->
    <fo:block xsl:use-attribute-sets="__frontmatter__title">
      <xsl:choose>
        <xsl:when test="$map/*[contains(@class,' topic/title ')][1]">
          <xsl:apply-templates select="$map/*[contains(@class,' topic/title ')][1]"/>
        </xsl:when>
        <xsl:when test="$map//*[contains(@class,' bookmap/mainbooktitle ')][1]">
          <xsl:apply-templates select="$map//*[contains(@class,' bookmap/mainbooktitle ')][1]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"/>
        </xsl:otherwise>
      </xsl:choose>
    </fo:block>
    <fo:block xsl:use-attribute-sets="__frontmatter__owner">
      <xsl:apply-templates select="$map//*[contains(@class,' bookmap/bookmeta ')]"/>
    </fo:block>
  </xsl:template>
  
  <xsl:template name="my:cover-image">
    <xsl:variable name="image"
        select="($map//*
            [contains(@class, ' topic/data ')]
            [@name = 'cover-image']/*[contains(@class, ' topic/image ')]
        )[1]" />
    <xsl:if test="$image">
      <xsl:apply-templates mode="placeImage" select="$image">
        <xsl:with-param name="imageAlign" select="$image/@align" />
        <xsl:with-param name="href" select="
            if ($image/@scope = 'external' or
                opentopic-func:isAbsolute($image/@href))
            then $image/@href
            else concat($input.dir.url, $image/@href)"
        />
        <xsl:with-param name="height" select="$image/@height" />
        <xsl:with-param name="width" select="$image/@width" />
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>

